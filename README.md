# Scrimp (Beta)
Scrimp is a ride share service sharing app for BRAC University students. The app lets you share Uber, Pathao, Shohoz or any other car ride share service with other users to minimize cost and maximize efficiency.

### Features
1. Create a Scrimp account using your email, phone, Bracu ID and other information

2. Pick your destination location form the university premises and wait for the app's match-making process to find other users also wiling to go to your nearby destination. The app will match up to 4 users. I will add an option to select maximum matched users up to 6 for services like Uber XL.

3. All matched users will be put in a realtime lobby where they can check each others profile, see their University ID cards.

4. Users will be able to chat with each other in the chat lobby.

### Components
- **SDK** : Android SDK
- **Language** : Java
- **Backend** : Google Firebase
- **Database**: Cloud Firestore, Realtime Database
- **Cloud Fuctions**: https://gitlab.com/RaJaKaR/scrimp-cloud-function
- **Libraries**: AndroidX, Lottie, CircularImageView
